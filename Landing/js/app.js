var shiponTaxi = angular.module('shiponTaxi', ['ui.bootstrap', 'ui.router']);

shiponTaxi.config(function($stateProvider, $urlRouterProvider) {
    
    $stateProvider
    .state({
        name: 'about',
        url: '/about',
        templateUrl: 'templates/about.html'
    })
    .state({
        name: 'cities',
        url: '/cities',
        templateUrl: 'templates/cities.html'
    })
    .state({
        name: 'contacts',
        url: '/contacts',
        templateUrl: 'templates/contacts.html'
    });
    
    $urlRouterProvider.otherwise('/about');
    
});

shiponTaxi.controller('headerController', ['$location', function($location) {
    
    var headCtrl = this;
    
    headCtrl.isPath = function(path) {
        return path === $location.path();
    };
    
}]);
